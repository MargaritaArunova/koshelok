package com.tinkoff.sirius.koshelok.controllers;

import com.tinkoff.sirius.koshelok.dto.CategoryDto;
import com.tinkoff.sirius.koshelok.dto.PersonDto;
import com.tinkoff.sirius.koshelok.services.CategoryService;
import com.tinkoff.sirius.koshelok.types.OperationType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;


@AutoConfigureMockMvc
@AutoConfigureWebMvc
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = CategoryController.class)
public class CategoryControllerTest {
    protected MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @MockBean
    private CategoryService categoryService;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void getAllIncomeCategoriesForPerson() throws Exception {
        var person = new PersonDto()
                .setEmail("sbeuhbv@mail.ru");
        var category = new CategoryDto()
                .setName("Kek")
                .setType(OperationType.INCOME);
        categoryService.createCategory(person.getEmail(), category);
        Mockito.when(categoryService.getAllCategoriesByType(person.getEmail(), OperationType.INCOME))
                .thenReturn(Collections.singletonList(category));
        mockMvc.perform(get("/category"))
                .andDo(print());
    }

}
