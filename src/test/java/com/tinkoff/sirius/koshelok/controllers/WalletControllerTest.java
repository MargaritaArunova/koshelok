package com.tinkoff.sirius.koshelok.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.tinkoff.sirius.koshelok.dto.WalletDto;
import com.tinkoff.sirius.koshelok.exceptions.NotFoundException;
import com.tinkoff.sirius.koshelok.models.Wallet;
import com.tinkoff.sirius.koshelok.services.WalletService;
import com.tinkoff.sirius.koshelok.types.CurrencyType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.junit.jupiter.api.Assertions.*;

@WebMvcTest(WalletController.class)
public class WalletControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WalletService walletService;

    @BeforeAll
    public static void setUp() {
        mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        writer = mapper.writer().withDefaultPrettyPrinter();
    }

    static ObjectMapper mapper;

    static ObjectWriter writer;

    @Test
    public void postWalletWhenIncorrectWalletGivenThrowsException() throws Exception {
        var walletWithoutName = new WalletDto()
                .setIncome(new BigDecimal(0))
                .setAmountLimit(new BigDecimal(0))
                .setBalance(new BigDecimal(0))
                .setSpendings(new BigDecimal(0))
                .setId(1L)
                .setCurrency(CurrencyType.EUR);
        long walletWithoutNamePersonId = 1L;

        var walletWithNegativeLimit = new WalletDto()
                .setIncome(new BigDecimal(0))
                .setName("Name")
                .setAmountLimit(new BigDecimal(-1))
                .setBalance(new BigDecimal(0))
                .setSpendings(new BigDecimal(0))
                .setId(1L)
                .setCurrency(CurrencyType.EUR);
        long walletWithNegativeLimitPersonId = 2L;


        var walletWithoutNameJson = writer.writeValueAsString(walletWithoutName);
        var walletWithNegativeLimitJson = writer.writeValueAsString(walletWithNegativeLimit);
        assertAll(
                () -> mockMvc.perform(post("/wallet")
                                .header("Authorization", walletWithoutNamePersonId)
                                .content(walletWithoutNameJson)
                                .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isBadRequest()),
                () -> mockMvc.perform(post("/wallet")
                                .header("Authorization", walletWithNegativeLimitPersonId)
                                .content(walletWithNegativeLimitJson)
                                .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isBadRequest())
        );
    }

    @Test
    void getWalletReturnsNotFoundWhenWalletNotFound() throws Exception {
        Long notFoundId = 1L;
        String notFoundEmail = "fbsjhb@mail.ru";
        when(walletService.getWalletByPersonId(notFoundId, notFoundEmail))
                .thenThrow(new NotFoundException(Wallet.class, notFoundId));
        var result = mockMvc.perform(get("/wallet/{not_found_id}", notFoundId)
                        .header("email", notFoundEmail))
                .andExpect(status().isNotFound());
    }

    @Test
    void putWalletReturnsNotFoundWhenWalletNotFound() throws Exception {
        Long notFoundId = -1L;
        String notFoundEmail = "fbsjhb@mail.ru";
        var wallet = new WalletDto()
                .setIncome(new BigDecimal(0))
                .setName("Name")
                .setAmountLimit(new BigDecimal(1))
                .setBalance(new BigDecimal(0))
                .setSpendings(new BigDecimal(0))
                .setId(notFoundId)
                .setCurrency(CurrencyType.EUR);
        var walletJson = writer.writeValueAsString(wallet);
        when(walletService.updateWallet(notFoundId, notFoundEmail, wallet))
                .thenThrow(new NotFoundException(Wallet.class, notFoundId));
        var result = mockMvc.perform(put("/wallet/{not_found_id}", notFoundId)
                        .header("email", notFoundEmail)
                        .content(walletJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void returnsOKWHenGetWalletByIdGetsValidId() throws Exception {
        Long validId = 1L;
       String validEmail = "dnkj@mail.ru";
        var wallet = new WalletDto()
                .setIncome(new BigDecimal(0))
                .setName("Name")
                .setAmountLimit(new BigDecimal(1))
                .setBalance(new BigDecimal(0))
                .setSpendings(new BigDecimal(0))
                .setId(validId)
                .setCurrency(CurrencyType.EUR);
        when(walletService.getWalletByPersonId(validId, validEmail))
                .thenReturn(wallet);
        var result = mockMvc.perform(get("/wallet/{id}", validId)
                        .header("email", validEmail))
                .andExpect(status().isOk())
                .andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        var actualResult = mapper.readValue(contentAsString, WalletDto.class);
        assertEquals(wallet, actualResult);
    }
}
