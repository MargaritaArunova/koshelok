package com.tinkoff.sirius.koshelok.exceptions;

import org.postgresql.util.PSQLException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import com.tinkoff.sirius.koshelok.models.Error;

@ControllerAdvice
public class ExceptionHandlers {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Error> handleMethodArgumentNotValidException(NotFoundException ex) {
        var status = HttpStatus.NOT_FOUND;
        return ResponseEntity.status(status).body(new Error(status.toString(), ex.getMessage()));
    }

    @ExceptionHandler(
            {
                    MissingServletRequestParameterException.class,
                    PSQLException.class,
                    MethodArgumentNotValidException.class,
                    HttpMessageNotReadableException.class,
                    PSQLException.class
            })
    public ResponseEntity<Error> handleBadRequestException(Exception e) {
        var status = HttpStatus.BAD_REQUEST;
        return ResponseEntity.status(status).body(new Error(status.toString(), e.getMessage()));
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<Error> handleAuthenticationException(AuthenticationException ex) {
        var status = HttpStatus.UNAUTHORIZED;
        return ResponseEntity.status(status).body(new Error(status.toString(), ex.getMessage()));
    }
}
