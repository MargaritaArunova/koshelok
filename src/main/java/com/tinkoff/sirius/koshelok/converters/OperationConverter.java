package com.tinkoff.sirius.koshelok.converters;

import com.tinkoff.sirius.koshelok.dto.OperationRequestDto;
import com.tinkoff.sirius.koshelok.dto.OperationResponseDto;
import com.tinkoff.sirius.koshelok.models.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class OperationConverter {

    private final CategoryConverter categoryConverter;

    public Operation convert(OperationRequestDto dto) {
        return new Operation()
                .setId(dto.getId())
                .setBalance(dto.getBalance())
                .setDate(dto.getDate())
                .setType(dto.getType());
    }

    public OperationResponseDto convert(Operation operation) {
        return new OperationResponseDto()
                .setId(operation.getId())
                .setWalletId(operation.getWallet().getId())
                .setBalance(operation.getBalance())
                .setDate(operation.getDate())
                .setCategoryDto(categoryConverter.convert(operation.getCategory()))
                .setType(operation.getType());
    }
}
