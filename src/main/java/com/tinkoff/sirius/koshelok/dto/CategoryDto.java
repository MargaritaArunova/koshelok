package com.tinkoff.sirius.koshelok.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tinkoff.sirius.koshelok.types.OperationType;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Accessors(chain = true)
public class CategoryDto {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    Long id;

    @NotBlank
    String name;

    OperationType type;

    @NotBlank
    String color;

    Long iconId;
}
