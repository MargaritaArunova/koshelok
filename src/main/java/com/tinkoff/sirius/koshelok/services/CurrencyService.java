package com.tinkoff.sirius.koshelok.services;

import com.tinkoff.sirius.koshelok.converters.CurrencyConverter;
import com.tinkoff.sirius.koshelok.dto.CurrencyDto;
import com.tinkoff.sirius.koshelok.repositories.CurrencyLogRepository;
import com.tinkoff.sirius.koshelok.repositories.CurrencyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class CurrencyService {
    private final List<String> dailyCurrenciesCodes = List.of("GBP", "EUR", "USD");

    private final CurrencyRepository currencyRepository;

    private final CurrencyLogRepository currencyLogRepository;

    private final Random rnd = new Random();

    private final CurrencyConverter currencyConverter;

    public List<CurrencyDto> getAllCurrencies() {
        return currencyRepository.findAll()
                .stream()
                .map(currencyConverter::convert)
                .collect(Collectors.toList());
    }

    public List<CurrencyDto> getDailyCurrencies() {
        List<CurrencyDto> dailyCurrencies = new ArrayList<>();
        for (var currency : currencyRepository.findCurrenciesByCodeIn(dailyCurrenciesCodes)) {
            var currencyDto = currencyConverter.convert(currency);
            if (currencyLogRepository.count() >= 2) {
                var currencies = currencyLogRepository.findFirst2ByOrderByIdDesc();
                if (currencies.size() >= 2) {
                    var lastCurrencyLog = currencies.get(0);
                    var previousCurrencyLog = currencies.get(1);
                    boolean ascending = true;
                    switch (currency.getCode()) {
                        case "USD" -> ascending = lastCurrencyLog.getUsd().compareTo(previousCurrencyLog.getUsd()) >= 0;
                        case "EUR" -> ascending = lastCurrencyLog.getEur().compareTo(previousCurrencyLog.getEur()) >= 0;
                        case "GBP" -> ascending = lastCurrencyLog.getGbp().compareTo(previousCurrencyLog.getGbp()) >= 0;
                    }
                    currencyDto.setAscending(ascending);
                }
            } else {
                currencyDto.setAscending(rnd.nextBoolean());
            }
            dailyCurrencies.add(currencyDto);
        }
        return dailyCurrencies;
    }
}
