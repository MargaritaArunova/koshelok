package com.tinkoff.sirius.koshelok.services;

import com.tinkoff.sirius.koshelok.clients.CbrCurrencyClient;
import com.tinkoff.sirius.koshelok.dto.CurrencyDto;
import com.tinkoff.sirius.koshelok.models.CurrencyLog;
import com.tinkoff.sirius.koshelok.repositories.CurrencyLogRepository;
import com.tinkoff.sirius.koshelok.repositories.CurrencyRepository;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

@AllArgsConstructor
@Service
public class DownloadCurrencyService {
    private final CbrCurrencyClient client;

    private final CurrencyRepository currencyRepository;

    private final CurrencyLogRepository currencyLogRepository;

    @Scheduled(cron = "0 0 0 * * *", zone = "Europe/Moscow")
    void updateCurrenciesValues() {
        var cbrCurrencyDtos = client.getCurrencyByDate(LocalDate.now());


        List<CurrencyDto> currencyDtos = cbrCurrencyDtos.stream().map(
                cbrCurrencyDto -> new CurrencyDto()
                        .setValue(cbrCurrencyDto.getCurrencyValue())
                        .setCode(cbrCurrencyDto.getCharCode())
        ).toList();
        for (var currencyDto : currencyDtos) {
            currencyRepository.save(currencyRepository.findByCode(currencyDto.getCode())
                    .setValue(currencyDto.getValue()));
        }

        var currencyLog = new CurrencyLog()
                .setDate(ZonedDateTime.now()
                        .withZoneSameInstant(ZoneId.of("Europe/Moscow"))
                        .toLocalDate());
        for (CurrencyDto currency : currencyDtos) {
            switch (currency.getCode()) {
                case "USD" -> currencyLog.setUsd(currency.getValue());
                case "EUR" -> currencyLog.setEur(currency.getValue());
                case "CHF" -> currencyLog.setChf(currency.getValue());
                case "GBP" -> currencyLog.setGbp(currency.getValue());
                case "JPY" -> currencyLog.setJpy(currency.getValue());
                case "SEK" -> currencyLog.setSek(currency.getValue());
            }
        }
        currencyLogRepository.save(currencyLog);
    }

    List<CurrencyDto> getCurrenciesValuesByDate(LocalDate date) {
        var cbrCurrencyDtos = client.getCurrencyByDate(date);

        return cbrCurrencyDtos.stream().map(
                cbrCurrencyDto -> new CurrencyDto()
                        .setValue(cbrCurrencyDto.getCurrencyValue())
                        .setCode(cbrCurrencyDto.getCharCode())
        ).toList();
    }
}
