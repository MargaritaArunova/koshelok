package com.tinkoff.sirius.koshelok.services;

import com.tinkoff.sirius.koshelok.converters.CategoryConverter;
import com.tinkoff.sirius.koshelok.dto.CategoryDto;
import com.tinkoff.sirius.koshelok.exceptions.AuthenticationException;
import com.tinkoff.sirius.koshelok.exceptions.NotFoundException;
import com.tinkoff.sirius.koshelok.models.*;
import com.tinkoff.sirius.koshelok.repositories.CategoryRepository;
import com.tinkoff.sirius.koshelok.repositories.PersonRepository;
import com.tinkoff.sirius.koshelok.types.OperationType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryConverter converter;

    private final CategoryRepository categoryRepository;

    private final PersonRepository personRepository;

    @Transactional
    public CategoryDto createCategory(String email, CategoryDto categoryDto) {
        var category = converter.convert(categoryDto);
        var person = personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );
        category.setPerson(personRepository.findById(person.getId()).orElseThrow(
                () -> new NotFoundException(Person.class, person.getId())
        ));
        return converter.convert(categoryRepository.save(category));
    }

    public CategoryDto getCategoryById(Long id, String email) {
        personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );
        var categoryOpt = categoryRepository.findById(id);
        if (categoryOpt.isEmpty()) {
            throw new NotFoundException(Category.class, id);
        }
        return converter.convert(categoryOpt.get());
    }

    @Transactional
    public CategoryDto updateCategory(CategoryDto categoryDto, Long categoryId, String email) {
        personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );
        var categoryOpt = categoryRepository.findById(categoryId);
        if (categoryOpt.isEmpty()) {
            throw new NotFoundException(Category.class, categoryId);
        }
        var category = categoryOpt.get();
        category.setColor(categoryDto.getColor())
                .setIconId(categoryDto.getIconId())
                .setName(categoryDto.getName())
                .setType(categoryDto.getType());
        return converter.convert(categoryRepository.save(category));
    }

    @Transactional
    public void deleteCategory(Long id, String email) {
        personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );
        categoryRepository.deleteById(id);
    }

    public List<CategoryDto> getAllCategoriesByType(String email, OperationType categoryType) {
        var person = personRepository.findByEmail(email).orElseThrow(
                () -> new AuthenticationException(email)
        );
        return categoryRepository.findCategoriesByPersonIdAndType(person.getId(), categoryType)
                .stream()
                .map(converter::convert)
                .toList();
    }
}
