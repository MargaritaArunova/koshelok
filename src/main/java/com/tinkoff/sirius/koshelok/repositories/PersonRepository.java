package com.tinkoff.sirius.koshelok.repositories;

import com.tinkoff.sirius.koshelok.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface PersonRepository extends JpaRepository<Person, Long> {
    public Optional<Person> findByEmail(String email);
}

