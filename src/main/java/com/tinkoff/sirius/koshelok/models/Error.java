package com.tinkoff.sirius.koshelok.models;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;
@Data
@Schema
public class Error {
    public Error(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private String code;
    @NotNull
    private String message;
}