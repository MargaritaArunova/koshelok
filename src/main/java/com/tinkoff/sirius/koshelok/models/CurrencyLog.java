package com.tinkoff.sirius.koshelok.models;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Accessors(chain = true)
@Entity(name = "currency_log")
@SequenceGenerator(allocationSize = 1, name = "currency_log_seq", sequenceName = "currency_log_seq")
public class CurrencyLog {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "currency_log_seq")
    private Long id;

    @CreatedDate
    private LocalDate date;

    @Column(name = "usd")
    private BigDecimal usd;

    @Column(name = "eur")
    private BigDecimal eur;

    @Column(name = "chf")
    private BigDecimal chf;

    @Column(name = "gbp")
    private BigDecimal gbp;

    @Column(name = "jpy")
    private BigDecimal jpy;

    @Column(name = "sek")
    private BigDecimal sek;
}
