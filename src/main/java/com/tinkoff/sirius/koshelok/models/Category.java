package com.tinkoff.sirius.koshelok.models;

import com.tinkoff.sirius.koshelok.types.OperationType;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
@Entity(name = "Category")
@SequenceGenerator(allocationSize = 1, name = "category_seq", sequenceName = "category_seq")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_seq")
    private Long id;

    @Column
    private String name;

    @Column
    @Enumerated(EnumType.STRING)
    private OperationType type;

    @Column
    private String color;

    @Column(name="icon_id")
    private Long iconId;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
    private List<Operation> operations = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id")
    private Person person;

}
